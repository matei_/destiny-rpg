/*
	
	@author: matei_
	@created at: 25.02.2021
	@last edit:

*/

#include <a_samp>

#include <a_mysql>
#include <crashdetect>
#include <easyDialog>
#include <sscanf2>
#include <streamer>

#define YSI_NO_HEAP_MALLOC
#define YSI_NO_VERSION_CHECK
#define YSI_NO_OPTIMISATION_MESSAGE
#define YSI_NO_MODE_CACHE

#include <YSI_Coding\y_hooks>
#include <YSI_Coding\y_timers>
#include <YSI_Data\y_iterate>
#include <YSI_Visual\y_commands>

// Configuratie MySQL
#define 	MYSQL_HOST		"localhost"
#define 	MYSQL_USER 		"root"
#define 	MYSQL_PASS		"mateidaniel20011402"
#define 	MYSQL_DATABASE 	"destiny"

new 
	MySQL: g_SQL, g_Query[256], s_String[100];

// Definitii generale
#define 	this::%0(%1) 	forward %0(%1); public %0(%1)
this::Kick_Ban(playerid, bool: kickban) return (!kickban) ? Kick(playerid) : Ban(playerid);
#define KickEx(%1) SetTimerEx("Kick_Ban", 500, false, "ii", %1, false)
#define getName(%0) playerVariables[%0][playerUsername]
#define getSQLID(%0) playerVariables[%0][playerSQLID]

#define SendUsageMessage(%0,%1) \
	SendFormatMessage(%0, COLOR_LRED, "SINTAXA: {FFFFFF}"%1)

#define SendErrorMessage(%0,%1) \
	SendFormatMessage(%0, COLOR_LRED, "EROARE: {FFFFFF}"%1)

#define SendServerMessage(%0,%1) \
	SendFormatMessage(%0, COLOR_SERVER, "SERVER: {FFFFFF}"%1)

#define SendMessage(%0,%1) \
	SendFormatMessage(%0, COLOR_WHITE, %1)

#define SendAdminMessage(%0,%1) \
	SendFormatMessage(%0, COLOR_LRED, "ADMCMD: {FFFFFF}"%1)

#define SendWarningMessage(%0,%1) \
	SendFormatMessage(%0, COLOR_LRED, "WARNING: {FFFFFF}"%1)

#define SendUnauthMessage(%0,%1) \
	SendFormatMessage(%0, COLOR_LRED, "NEAUTORIZAT: {FFFFFF}"%1)


// Definitii server
#define 	SERVER_VERSION 		"release 25.02.21a"

#define 	MAX_LOGIN_ATTEMPTS	3
#define 	MAX_REGION_SKINS 	4
#define 	INDEX_UTILITIES 	1

// Culori
#define 	COLOR_SERVER 		0xA2B4C3FF
#define 	COLOR_RED 			0xBA2424FF
#define 	COLOR_GREY			0xAFAFAFAA
#define 	COLOR_DYELLOW		0xFFC266AA
#define 	COLOR_CYAN 			0x5EB1B1FF
#define 	COLOR_YELLOW 		0xFFFF00FF
#define 	COLOR_WHITE 		0xFFFFFFFF
#define 	COLOR_PURPLE		0xD0AEEBFF
#define 	COLOR_LRED			0xFF6347AA
#define 	COLOR_BLUE			0x2641FEFF
#define 	COLOR_GRAD6 		0xF0F0F0FF
#define 	COLOR_BROWN 		0x993300AA
#define 	COLOR_GREEN 		0x33AA33AA
#define 	COLOR_WHITE 		0xFFFFFFFF
#define 	COLOR_RED2			0xD34747FF



// Variabile
new
	loginAttempts[MAX_PLAYERS], isPlayerLogged[MAX_PLAYERS], registerSkin[MAX_PLAYERS], registerSex[MAX_PLAYERS],
	playerRegion[MAX_PLAYERS], g_MySQLRaceCheck[MAX_PLAYERS];

new
	Timer: loginTimer[MAX_PLAYERS];

new
	Iterator: playersInConnect<MAX_PLAYERS>, Iterator: Admins<MAX_PLAYERS>;

new
	PlayerText: registerPTD[MAX_PLAYERS][MAX_REGION_SKINS], Text: registerTD[4], Text: registerRegion[4], Text: registerGender[2];

// Enums
enum accountEnums {
	playerSQLID, playerUsername[MAX_PLAYER_NAME], playerPassword[65], playerSalt[17], playerAdmin, playerRegistered,
	playerEmail[40], playerAge, playerGender, playerSkin, Cache: playerCacheID
};
new
	playerVariables[MAX_PLAYERS][accountEnums];

enum regionEnum {	maleModel[4], femaleModel[4]	};

new regionVariable[ 4 ][ regionEnum ] = {

	{ { 60, 133, 170, 37 }, { 56, 131, 193, 190 } },
	{ { 15, 7, 25, 144 }, { 13, 12, 243, 10 } },
	{ { 20, 29, 44, 72 }, { 88, 91, 199, 216 } },
	{ { 229, 210, 58, 203 }, { 225, 263, 53, 214 } }

};

main() {
	print("Server is loading, please wait...");
	print("Script created by: @matei_");
	printf("Script version: %s\n", SERVER_VERSION);
}

public OnGameModeInit() {
	// Conexiune MySQL
	new MySQLOpt: option_id = mysql_init_options();
	mysql_set_option(option_id, AUTO_RECONNECT, true);

	mysql_log(ERROR | WARNING);
	g_SQL = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DATABASE, option_id);
	if(g_SQL == MYSQL_INVALID_HANDLE || mysql_errno(g_SQL) != 0) {
		printf("ERROR: Could not connect to MySQL (HOST: %s | DB: %s | USER: %s)", MYSQL_HOST, MYSQL_DATABASE, MYSQL_USER);
		print("HINT: Make sure that you provided the correct connection credentials.");
		printf("HINT: Error number: %i", mysql_errno(g_SQL));
	} else print("Connection successful toward MySQL Database Server.");

	// Initializare
	SetGameModeText(SERVER_VERSION);
	AddPlayerClass(0, 1958.3783, 1343.1572, 15.3746, 269.1425, 0, 0, 0, 0, 0, 0);

	loadObjects();
	createTextdraws();
	return true;
}

public OnGameModeExit() {
	for(new i = 0, j = GetPlayerPoolSize(); i <= j; i ++) {
		if(IsPlayerConnected(i)) {
			OnPlayerDisconnect(i, 1);
		}
	}
	mysql_close(g_SQL);
	return true;
}

stock SendFormatMessage(playerid, color, const fmat[], va_args<>) {
	va_format(s_String, sizeof(s_String), fmat, va_start<3>);
	return SendClientMessage(playerid, color, s_String);
}

public OnPlayerConnect(playerid) {
	g_MySQLRaceCheck[playerid] ++;

	static const empty_player[accountEnums];
	playerVariables[playerid] = empty_player;

	GetPlayerName(playerid, playerVariables[playerid][playerUsername], MAX_PLAYER_NAME);

	SendServerMessage(playerid, "Iti verificam numele, te rog asteapta...");

	g_Query[0] = EOS;
	mysql_format(g_SQL, g_Query, sizeof(g_Query), "SELECT * FROM `accounts` WHERE `playerUsername` = '%e' LIMIT 1", playerVariables[playerid][playerUsername]);
	mysql_tquery(g_SQL, g_Query, "checkPlayerAccount", "dd", playerid, g_MySQLRaceCheck[playerid]);
	return true;
}

public OnPlayerDisconnect(playerid, reason) {
	g_MySQLRaceCheck[playerid] ++;

	if(cache_is_valid(playerVariables[playerid][playerCacheID])) {
		cache_delete(playerVariables[playerid][playerCacheID]);
		playerVariables[playerid][playerCacheID] = MYSQL_INVALID_CACHE;
	}

	if(Iter_Contains(playersInConnect, playerid)) {
		stop loginTimer[playerid];
		loginAttempts[playerid] = 0;
		isPlayerLogged[playerid] = 0;
		Iter_Remove(playersInConnect, playerid);
	}
	return true;
}

this::checkPlayerAccount(playerid, race_check) {
	if(race_check != g_MySQLRaceCheck[playerid])
		return KickEx(playerid);

	new checkString[145];
	if(cache_num_rows() > 0) {
		cache_get_value(0, "playerPass", playerVariables[playerid][playerPassword], 65);
		cache_get_value(0, "playerSalt", playerVariables[playerid][playerSalt], 17);

		playerVariables[playerid][playerCacheID] = cache_save();

		format(checkString, sizeof(checkString), "Acest cont (%s) a fost gasit in baza noastra de date. Te rugam sa te loghezi introducand parola contului in casuta de mai jos:", getNameEx(playerid));
		Dialog_Show(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "SERVER: Logare in cont", checkString, "Login", "Anuleaza");
		loginCamera(playerid);
	} else {
		format(checkString, sizeof(checkString), "Bine ai venit %s, te poti inregistra introducand parola contului in casuta de mai jos:", getNameEx(playerid));
		Dialog_Show(playerid, DIALOG_REGISTER, DIALOG_STYLE_PASSWORD, "SERVER: Inregistrare cont", checkString, "Urmatorul", "Anuleaza");
	}

	loginTimer[playerid] = defer playerLoginTimer(playerid);
	Iter_Add(playersInConnect, playerid);
	createPlayerTextdraws(playerid);
	return true;
}

timer playerLoginTimer[1000 * 60](playerid)
	return kickPlayer(playerid, "Timpul pentru logare / inregistrare (60 de secunde) s-a scurs.");

Dialog:DIALOG_LOGIN(playerid, response, listitem, inputtext[]) {
	if(!response)
		return kickPlayer(playerid, "Ai anulat procesul de login.");

	new hashed_pass[65];
	SHA256_PassHash(inputtext, playerVariables[playerid][playerSalt], hashed_pass, 65);

	if(strcmp(hashed_pass, playerVariables[playerid][playerPassword]) == 0) {
		cache_set_active(playerVariables[playerid][playerCacheID]);

		onPlayerLogin(playerid);

		cache_delete(playerVariables[playerid][playerCacheID]);
		playerVariables[playerid][playerCacheID] = MYSQL_INVALID_CACHE;
	} else {
		loginAttempts[playerid] ++;
		SendFormatMessage(playerid, COLOR_RED, "Parola pe care ai introdus-o este gresita! (%i incercari ramase)", 3 - loginAttempts[playerid]);

		if(loginAttempts[playerid] == MAX_LOGIN_ATTEMPTS) return kickPlayer(playerid, "Ai folosit toate incercarile ramase pentru introducerea parolei."), insertLogin(playerid, false);
		else Dialog_Show(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "SERVER: Logare in cont (parola gresita)", "Parola introdusa este gresita!\nTe rugam sa iti introduci parola in casuta de mai jos:", "Logare", "Anuleaza");
	}
	return true;
}

this::onPlayerLogin(playerid) {
	cache_get_value_name_int(0, "playerSQLID", playerVariables[playerid][playerSQLID]);

	cache_get_value_name_int(0, "playerAdmin", playerVariables[playerid][playerAdmin]);
	cache_get_value_name_int(0, "playerRegistered", playerVariables[playerid][playerRegistered]);
	cache_get_value_name_int(0, "playerAge", playerVariables[playerid][playerAge]);
	cache_get_value_name_int(0, "playerGender", playerVariables[playerid][playerGender]);
	cache_get_value_name_int(0, "playerSkin", playerVariables[playerid][playerSkin]);

	cache_get_value_name(0, "playerEmail", playerVariables[playerid][playerEmail], 40);

	stop loginTimer[playerid];

	if(playerVariables[playerid][playerRegistered] < 3) {
		SendServerMessage(playerid, "Te rugam sa completez procesul de inregistrare al contului.");
		completeRegister(playerid);
	} else {
		SpawnPlayer(playerid);
		Iter_Remove(playersInConnect, playerid);
		isPlayerLogged[playerid] = 1;
		loginAttempts[playerid] = 0;

		RemovePlayerAttachedObject(playerid, INDEX_UTILITIES);
		TogglePlayerControllable(playerid, true), SetCameraBehindPlayer(playerid);
		SendServerMessage(playerid, "Te-ai logat cu succes pe server.");
		insertLogin(playerid, true);

		if(playerVariables[playerid][playerAdmin] > 0) {
			format(s_String, sizeof(s_String), "(A) HelloBot: %s tocmai s-a logat pe server.", getName(playerid));
			SendAdminsMessage(COLOR_DYELLOW, 1, s_String);
			Iter_Add(Admins, playerid);
		}		
	}
	return true;
}

this::insertLogin(playerid, bool: status) {
	g_Query[0] = EOS;

	if(status == true) mysql_format(g_SQL, g_Query, sizeof(g_Query), "INSERT INTO `login_logs` (`playerName`, `playerIP`, `playerStatus`) VALUES ('%e', '%e', '1')", getNameEx(playerid), getPlayerIP(playerid));
	else mysql_format(g_SQL, g_Query, sizeof(g_Query), "INSERT INTO `login_logs` (`playerName`, `playerIP`, `playerStatus`) VALUES ('%e', '%e', '2')", getNameEx(playerid), getPlayerIP(playerid));
	mysql_tquery(g_SQL, g_Query, "", "");

	return true;
}

Dialog:DIALOG_REGISTER(playerid, response, listitem, inputtext[]) {
	if(!response)
		return kickPlayer(playerid, "Ai anulat procesul de inregistrare al contului.");

	if(strlen(inputtext) <= 5)
		return Dialog_Show(playerid, DIALOG_REGISTER, DIALOG_STYLE_PASSWORD, "SERVER: Inregistrare cont", "Parola contului tau trebuie sa fie mai lunga de 5 caractere!\nTe rugam introdu parola contului tau in casuta de mai jos:", "Urmatoru", "Anuleaza");

	for(new i = 0; i < 16; i ++) playerVariables[playerid][playerSalt][i] = random(94) + 33;
	SHA256_PassHash(inputtext, playerVariables[playerid][playerSalt], playerVariables[playerid][playerPassword], 65);

	g_Query[0] = EOS;
	mysql_format(g_SQL, g_Query, sizeof(g_Query), "INSERT INTO `accounts` (`playerUsername`, `playerPass`, `playerSalt`, `playerIP`) VALUES ('%e', '%e', '%e', '%e')", getNameEx(playerid), playerVariables[playerid][playerPassword], playerVariables[playerid][playerSalt], getPlayerIP(playerid));
	mysql_tquery(g_SQL, g_Query, "onPlayerRegister", "i", playerid);

	return true;
}

this::onPlayerRegister(playerid) {
	stop loginTimer[playerid];
	playerVariables[playerid][playerSQLID] = cache_insert_id();

	Dialog_Show(playerid, DIALOG_R_EMAIL, DIALOG_STYLE_INPUT, "Server: Inregistrare e-mail", "Un e-mail valid te va ajuta sa iti resetezi sau sa iti recuperezi parola, in cazul in care ai pierdut-o.\nTe rugam introdu un e-mail valid in casuta de mai jos:", "Urmatorul", "Anuleaza");
	return true;
}

Dialog:DIALOG_R_EMAIL(playerid, response, listitem, inputtext[]) {
	if(!response)
		return kickPlayer(playerid, "Nu ai completat procesul de inregistrare al contului.");
	if(playerVariables[playerid][playerRegistered] == 1)
		return KickEx(playerid);
	if(strlen(inputtext) < 13 || strlen(inputtext) > 40)
		return Dialog_Show(playerid, DIALOG_R_EMAIL, DIALOG_STYLE_INPUT, "SERVER: Inregistrare e-mail", "{D34747}EROARE:{FFFFFF} E-mail invalid!\nUn e-mail valid te va ajuta sa iti resetezi sau sa iti recuperezi parola, in cazul in care ai pierdut-o.\nTe rugam introdu un e-mail valid in casuta de mai jos:", "Urmatorul", "Anuleaza");
	if(!isMail(inputtext))
		return Dialog_Show(playerid, DIALOG_R_EMAIL, DIALOG_STYLE_INPUT, "SERVER: Inregistrare e-mail", "{D34747}EROARE:{FFFFFF} E-mail invalid!\nUn e-mail valid te va ajuta sa iti resetezi sau sa iti recuperezi parola, in cazul in care ai pierdut-o.\nTe rugam introdu un e-mail valid in casuta de mai jos:", "Urmatorul", "Anuleaza");

	g_Query[0] = EOS;
	mysql_format(g_SQL, g_Query, sizeof(g_Query), "UPDATE `accounts` SET `playerEmail` = '%s', `playerRegistered` = '1' WHERE `playerSQLID` = '%i'", inputtext, getSQLID(playerid));
	mysql_tquery(g_SQL, g_Query, "", "");

	Dialog_Show(playerid, DIALOG_R_AGE, DIALOG_STYLE_INPUT, "SERVER: Inregistrare varsta", "Te rugam introdu varsta caracterului in casuta de mai jos:", "Urmatorul", "Anuleaza");
	playerVariables[playerid][playerRegistered] = 1;

	return true;
}

Dialog:DIALOG_R_AGE(playerid, response, listitem, inputtext[]) {
	if(!response)
		return kickPlayer(playerid, "Nu ai completat procesul de inregistrare al contului.");
	if(playerVariables[playerid][playerRegistered] == 2)
		return KickEx(playerid);
	if(strval(inputtext) < 7 || strval(inputtext) > 50)
		return Dialog_Show(playerid, DIALOG_R_AGE, DIALOG_STYLE_INPUT, "SERVER: Inregistrare varsta", "{D34747}EROARE:{FFFFFF} Varsta invalida! (minim 7 ani, maxim 50 de ani)\nTe rugam introdu varsta caracterului in casuta de mai jos:", "Urmatorul", "Anuleaza");
	if(!isNumeric(inputtext))
		return Dialog_Show(playerid, DIALOG_R_AGE, DIALOG_STYLE_INPUT, "SERVER: Inregistrare varsta", "{D34747}EROARE:{FFFFFF} Varsta invalida!\nTe rugam introdu varsta caracterului in casauta de mai jos:", "Urmatorul", "Anuleaza");

	g_Query[0] = EOS;
	mysql_format(g_SQL, g_Query, sizeof(g_Query), "UPDATE `accounts` SET `playerAge` = '%d', `playerRegistered` = '2' WHERE `playerSQLID` = '%i'", strval(inputtext), getSQLID(playerid));
	mysql_tquery(g_SQL, g_Query, "", "");

	chooseCharacter(playerid);
	playerVariables[playerid][playerRegistered] = 2;

	return true;
}

stock chooseCharacter(playerid) {
	SpawnPlayer(playerid);
	TogglePlayerControllable(playerid, false), SetPlayerVirtualWorld(playerid, playerid + 1);
	SetPlayerPos(playerid, 1433.6111, -1228.3667, 152.3983), SetPlayerFacingAngle(playerid, 27.7050);
	SetPlayerCameraPos(playerid, 1432.4786, -1226.9811, 153.3983), SetPlayerCameraLookAt(playerid, 1434.3622, -1228.5782, 152.4143);
	SetPlayerInterior(playerid, 1);

	for(new i = 0; i < 20; i ++) SendClientMessage(playerid, -1, "");

	for(new i = 0; i < sizeof(registerTD); i ++) TextDrawShowForPlayer(playerid, registerTD[i]);
	for(new i = 0; i < MAX_REGION_SKINS; i ++) {
		PlayerTextDrawSetPreviewModel(playerid, registerPTD[playerid][i], regionVariable[0][maleModel][i]);
		PlayerTextDrawShow(playerid, registerPTD[playerid][i]);
	}
	for(new i = 0; i < sizeof(registerRegion); i ++) TextDrawShowForPlayer(playerid, registerRegion[i]);
	TextDrawShowForPlayer(playerid, registerGender[0]), TextDrawShowForPlayer(playerid, registerGender[1]);
	SelectTextDraw(playerid, 0x959595FF), SetPlayerSkin(playerid, 60);

	registerSkin[playerid] = registerSex[playerid] = playerRegion[playerid] = 0;

	return true;
}

stock loginCamera(playerid, bool: atRegister = false) {
	TogglePlayerControllable(playerid, false);
	SetPlayerPos(playerid, 1432.9116, -1230.6536, 152.3983), SetPlayerFacingAngle( playerid, 181.1182);
	SetPlayerVirtualWorld(playerid, playerid + 1), SetPlayerInterior( playerid, 1);
	SetPlayerCameraPos(playerid, 1431.7656, -1228.6426, 153.3983), SetPlayerCameraLookAt(playerid, 1432.9116, -1230.6536, 152.3983);

	SetPlayerSpecialAction(playerid, SPECIAL_ACTION_USECELLPHONE);
	SetPlayerAttachedObject(playerid, INDEX_UTILITIES, 330, 6, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1.000000, 1.000000, 1.000000);

	if(atRegister == false) {
		if(!registerSex[playerid]) SetPlayerSkin(playerid, regionVariable[0][maleModel][0]);
		else SetPlayerSkin(playerid, regionVariable[0][femaleModel][0]);
	}

	return true;
}

public OnPlayerClickTextDraw(playerid, Text:clickedid) {
	for(new i = 0; i < sizeof(registerRegion); i ++) {
		if(clickedid == registerRegion[i]) {
			playerRegion[playerid] = i;

			if(!registerSex[playerid]) {
				for(new x = 0; x < MAX_REGION_SKINS; x ++) {
					PlayerTextDrawSetPreviewModel(playerid, registerPTD[playerid][x], regionVariable[i][maleModel][x]);
					PlayerTextDrawShow(playerid, registerPTD[playerid][x]);
				}
				SetPlayerSkin(playerid, regionVariable[i][maleModel][registerSkin[playerid]]);
			} else {
				for(new x = 0; x < MAX_REGION_SKINS; x ++) {
					PlayerTextDrawSetPreviewModel(playerid, registerPTD[playerid][x], regionVariable[i][femaleModel][x]);
					PlayerTextDrawShow(playerid, registerPTD[playerid][x]);
				}
				SetPlayerSkin(playerid, regionVariable[i][femaleModel][registerSkin[playerid]]);
			}
		}
	}

	for(new i = 0; i < sizeof(registerGender); i ++) {
		if(clickedid == registerGender[i]) {
			registerSex[playerid] = i;

			if(!registerSex[playerid]) {
				for(new x = 0; x < MAX_REGION_SKINS; x ++) {
					PlayerTextDrawSetPreviewModel(playerid, registerPTD[playerid][x], regionVariable[i][maleModel][x]);
					PlayerTextDrawShow(playerid, registerPTD[playerid][x]);
				}
				SetPlayerSkin(playerid, regionVariable[i][maleModel][registerSkin[playerid]]);
			} else {
				for(new x = 0; x < MAX_REGION_SKINS; x ++) {
					PlayerTextDrawSetPreviewModel(playerid, registerPTD[playerid][x], regionVariable[i][femaleModel][x]);
					PlayerTextDrawShow(playerid, registerPTD[playerid][x]);
				}
				SetPlayerSkin(playerid, regionVariable[i][femaleModel][registerSkin[playerid]]);
			}
		}
	}

	if(clickedid == registerTD[3]) {
		g_Query[0] = EOS;
		if(!registerSex[playerid])	mysql_format(g_SQL, g_Query, sizeof(g_Query), "UPDATE `accounts` SET `playerGender` = '1', `playerSkin` = '%d', `playerRegistered` = '3' WHERE `playerSQLID` = '%i'",
			regionVariable[playerRegion[playerid]][maleModel][registerSkin[playerid]], getSQLID(playerid));
		else mysql_format(g_SQL, g_Query, sizeof(g_Query), "UPDATE `accounts` SET `playerGender` = '2', `playerSkin` = '%d', `playerRegistered` = '3' WHERE `playerSQLID` = '%i'",
			regionVariable[playerRegion[playerid]][femaleModel][registerSkin[playerid]], getSQLID(playerid));
		mysql_tquery(g_SQL, g_Query, "", "");

		format(s_String, sizeof(s_String), "Cont nou: %s [SQLID: %i].", getNameEx(playerid), getSQLID(playerid));
		SendAdminsMessage(COLOR_RED, 1, s_String);

		Dialog_Show(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "SERVER: Loagre in cont", "Contul a fost inregistrat cu succes!\nTe rugam sa te loghezi introducand parola contului in casuta de mai jos:", "Login", "Anuleaza");
		playerVariables[playerid][playerRegistered] = 3;
		CancelSelectTextDraw(playerid), loginCamera(playerid, true);

		for(new i = 0; i < sizeof(registerTD); i ++) TextDrawHideForPlayer(playerid, registerTD[i]);
		for(new i = 0; i < MAX_REGION_SKINS; i ++) PlayerTextDrawHide(playerid, registerPTD[playerid][i]);
		for(new i = 0; i < sizeof(registerRegion); i ++) TextDrawHideForPlayer(playerid, registerRegion[i]);
		TextDrawHideForPlayer(playerid, registerGender[0]), TextDrawHideForPlayer(playerid, registerGender[1]);
	}

	return true;
}

public OnPlayerClickPlayerTextDraw(playerid, PlayerText:playertextid) {
	for(new i = 0; i < MAX_REGION_SKINS; i ++) {
		if(playertextid == registerPTD[playerid][i]) {
			registerSkin[playerid] = i;

			if(!registerSex[playerid]) SetPlayerSkin(playerid, regionVariable[playerRegion[playerid]][maleModel][i]);
			else SetPlayerSkin(playerid, regionVariable[playerRegion[playerid]][femaleModel][i]);
		}
	}

	return true;
}

stock completeRegister(playerid) {
	switch(playerVariables[playerid][playerRegistered]) {
		case 0:	Dialog_Show(playerid, DIALOG_R_EMAIL, DIALOG_STYLE_INPUT, "SERVER: Inregistrare e-mail", "Un e-mail valid te va ajuta sa iti resetezi sau sa iti recuperezi parola, in cazul in care ai pierdut-o.\nTe rugam introdu un e-mail valid in casuta de mai jos:", "Urmatorul", "Anuleaza");
		case 1:	Dialog_Show(playerid, DIALOG_R_AGE, DIALOG_STYLE_INPUT, "SERVER: Inregistrare varsta", "Te rugam introdu varsta caracterului in casuta de mai jos:", "Urmatorul", "Anuleaza");
		case 2: chooseCharacter(playerid);
	}

	return true;
}

public OnPlayerSpawn(playerid) {
	if(isPlayerLogged[playerid]) {
		SetPlayerSkin(playerid, playerVariables[playerid][playerSkin]);
		SetPlayerVirtualWorld(playerid, 0), SetPlayerInterior(playerid, 0);
	}

	return true;
}

public OnPlayerRequestClass(playerid, classid) return true;

public e_COMMAND_ERRORS:OnPlayerCommandReceived(playerid, cmdtext[], e_COMMAND_ERRORS:success) {
	if(success == COMMAND_UNDEFINED) {
		SendClientMessage(playerid, -1, "Aceasta comanda nu exista, foloseste [/help] pentru mai multe informatii.");
		return COMMAND_OK;
	}
	return COMMAND_OK;
}

stock getNameEx(playerid) {
	new playerName[MAX_PLAYER_NAME];
	GetPlayerName(playerid, playerName, MAX_PLAYER_NAME);
	return playerName;
}

stock getPlayerIP(playerid) {
	new playerIP[16];
	GetPlayerIp(playerid, playerIP, sizeof(playerIP));
	return playerIP;
}

stock isMail(const email[]) {
	new mailLength = strlen(email), cstate = 0;
	for(new i = 0; i < mailLength; i ++) {
		if((cstate == 0 || cstate == 1) && (email[i] >= 'A' && email[i] <= 'Z') || (email[i] >= '0' && email[i] <= '9') || (email[i] >= 'a' && email[i] <= 'z') || (email[i] == '.') || (email[i] == '-') || (email[i] == '_')) { }
		else {
			if((cstate == 0) && (email[i] == '@')) cstate = 1;
			else return false;
		}
	}

	if(cstate < 1) return false;
	if(mailLength < 6) return false;
	if((email[mailLength - 3] == '.') || (email[mailLength - 4] == '.') || (email[mailLength - 5] == '.')) return true;
	return false;
}

isNumeric(const string[]) {
	for(new i = 0, j = strlen(string); i < j; i ++) {
		if(string[i] > '9' || string[i] < '0') return false;
	}
	return true;
}

stock SendAdminsMessage(color, fromLevel, const message[]) {
	foreach(new i : Admins) if(isPlayerLogged[i] && playerVariables[i][playerAdmin] >= fromLevel) SendClientMessage(i, color, message);

	return true;
}

stock isLogged(playerid) {
	if(!IsPlayerConnected(playerid) || !isPlayerLogged[playerid] || playerid == INVALID_PLAYER_ID) return false;

	return true;
}

this::kickPlayer(playerid, const reason[]) {
	if(GetPVarInt(playerid, "gotKicked")) return true;

	SendClientMessage(playerid, COLOR_RED, reason);
	SetPVarInt(playerid, "gotKicked", 1);
	KickEx(playerid);

	return true;
}

this::updateAccountInt(playerid, const varname[], amount) {
	if(playerid == INVALID_PLAYER_ID || !isPlayerLogged[playerid]) return true;

	g_Query[0] = EOS;
	mysql_format(g_SQL, g_Query, sizeof(g_Query), "UPDATE `accounts` SET `%s` = '%d' WHERE `playerSQLID` = '%i'", varname, amount, getSQLID(playerid));
	mysql_tquery(g_SQL, g_Query, "", "");

	return true;
}

// Comenzi
YCMD:admins(playerid, params[], help) {
	new adminString[40];
	SendClientMessage(playerid, COLOR_CYAN, "------------------------ Online admins ------------------------");
	foreach(new i : Admins) {
		format(adminString, sizeof(adminString), "%s - admin level %i", getName(i), playerVariables[playerid][playerAdmin]);
		SendClientMessage(playerid, -1, adminString);
	}
	SendClientMessage(playerid, COLOR_CYAN, "--------------------------------------------------------------------");

	return true;
}

YCMD:setadmin(playerid, params[], help) {
	if(playerVariables[playerid][playerAdmin] < 6)
		return SendUnauthMessage(playerid, "Nu poti folosi aceasta comanda, deoarece nu ai admin level 6+.");

	new playerID, adminLevel, adminReason[24];
	if(sscanf(params, "uis[24]", playerID, adminLevel, adminReason))
		return SendUsageMessage(playerid, "/setadmin [playerID / playerName] [adminLevel] [motiv]");

	if(adminLevel < 0 || adminLevel > 7)
		return SendErrorMessage(playerid, "Admin level-ul trebuie sa fie cuprins intre 0 si 7.");

	if(!isLogged(playerID))
		return SendErrorMessage(playerid, "Jucatorul ales nu este conectat.");

	new adminString[120];
	if(playerVariables[playerID][playerAdmin] < adminLevel) {
		format(adminString, sizeof(adminString), "Ai fost promovat la admin level %i de catre %s, motiv: %s.", adminLevel, getName(playerid), adminReason), SendClientMessage(playerid, -1, adminString);
		format(adminString, sizeof(adminString), "%s a fost promovat la admin level %i de catre %s, motiv: %s.", getName(playerID), adminLevel, getName(playerid), adminReason);
	} else {
		format(adminString, sizeof(adminString), "Ai fost retrogradat la admin %i de catre %s, motiv: %s.", adminLevel, getName(playerid), adminReason), SendClientMessage(playerid, -1, adminString);
		format(adminString, sizeof(adminString), "%s a fost retrogradat la admin level %i de catre %s, motiv: %s.", getName(playerID), adminLevel, getName(playerid), adminReason);
	}
	SendAdminsMessage(COLOR_YELLOW, 1, adminString);

	g_Query[0] = EOS;
	mysql_format(g_SQL, g_Query, sizeof(g_Query), "INSERT INTO `staff_logs` (`logText`, `logType`) VALUES ('%s', '1')", adminString);
	mysql_tquery(g_SQL, g_Query, "", "");

	if(adminLevel > 0 && !Iter_Contains(Admins, playerID)) Iter_Add(Admins, playerID);
	else if(adminLevel == 0 && Iter_Contains(Admins, playerID)) Iter_Remove(Admins, playerID);

	playerVariables[playerid][playerAdmin] = adminLevel, updateAccountInt(playerid, "playerAdmin", adminLevel);

	return true;
}

#include <system\serverObjects>
#include <system\serverTextdraws>